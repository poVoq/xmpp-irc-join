Works by adding ```?c=examlechannel&h=examplenetwork``` to the url without the # for the channel

Currently included Biboumi gateways:
- irc.jabber.hot-chilli.net
- irc.hmm.st
- irc.jabberfr.org
- irc.cheogram.com
- irc.disroot.org
- irc.snopyta.org